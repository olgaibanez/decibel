Changelog
==================

v1.0
----
Functions included in ``decibel``:
 * ```enge_transcriptional_noise```
 * ```distance_to_celltype_mean```
 * ```enge_euclidean_dist```
 * ```pairwise_euclidean_sample```
 * ```hernando_herraez```
 * ```distance_to_celltype_mean_invariant```
 * ```gcl```
 * ```gcl_per_cell_type_and_batch```
 * ```rerun_preprocessing```
 * ```scallop_pipeline```
 
