API
=======

Decibel functions
-----------------


.. autofunction:: module.decibel.enge_transcriptional_noise
.. autofunction:: module.decibel.distance_to_celltype_mean
.. autofunction:: module.decibel.enge_euclidean_dist
.. autofunction:: module.decibel.pairwise_euclidean_sample
.. autofunction:: module.decibel.hernando_herraez
.. autofunction:: module.decibel.distance_to_celltype_mean_invariant
.. autofunction:: module.decibel.gcl
.. autofunction:: module.decibel.gcl_per_cell_type_and_batch
.. autofunction:: module.decibel.rerun_preprocessing
.. autofunction:: module.decibel.scallop_pipeline


