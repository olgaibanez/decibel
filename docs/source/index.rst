.. decibel documentation master file, created by
   sphinx-quickstart on Thursday July  28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Decibel
====================================================================

Measuring transcriptional noise in single-cell data. Check our manuscript `Lack of evidence for increased transcriptional noise in aged tissues <https://www.biorxiv.org/content/10.1101/2022.05.18.492432v1>`_



.. toctree::
   :hidden:
   
   self

   
.. toctree::
   :maxdepth: 2
   :hidden:
   
   personal-rst/installation  
   personal-rst/tutorial
   modules
   personal-rst/changelog  
   personal-rst/license
